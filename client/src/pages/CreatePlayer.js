import React, { Component } from "react";
import Email from "../components/email";
import Experience from "../components/experience";
import Lvl from "../components/lvl";
import Password from "../components/password";
import Username from "../components/username";

class CreatePlayer extends Component {
    state = {
        username: "",
        email: "",
        password: "",
        experience: "",
        lvl: "",
        showSummary: false,
    };

    handleFieldOnChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
            showSummary: false,
        });
    };

    handleShowSummary = () => {
        this.setState({ showSummary: true });
    };

    render() {
        return (
            <div class="container">
                <h1>Form Create Player</h1>
                <div class="container">
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Username
                                </label>
                                <div class="col-sm-10">
                                    <Username
                                        name="username"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="username"
                                        value={this.state.username}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Email
                                </label>
                                <div class="col-sm-10">
                                    <Email
                                        name="email"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="email"
                                        value={this.state.email}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Password
                                </label>
                                <div class="col-sm-10">
                                    <Password
                                        name="password"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="password"
                                        value={this.state.password}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Experience
                                </label>
                                <div class="col-sm-10">
                                    <Experience
                                        name="experience"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="experience"
                                        value={this.state.experience}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Lvl
                                </label>
                                <div class="col-sm-10">
                                    <Lvl
                                        name="lvl"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="lvl"
                                        value={this.state.lvl}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button
                    class="btn btn-primary"
                    onClick={this.handleShowSummary}
                >
                    Submit
                </button>
                <br />
                {this.state.showSummary && (
                    <ul>
                        <li>Username: {this.state.username}</li>
                        <li>Email: {this.state.email}</li>
                        <li>Experience: {this.state.experience}</li>
                        <li>Lvl: {this.state.lvl}</li>
                    </ul>
                )}
            </div>
        );
    }
}

export default CreatePlayer;
