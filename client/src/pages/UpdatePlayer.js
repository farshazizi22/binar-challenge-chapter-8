import React, { Component } from "react";
import Email from "../components/email";
import Experience from "../components/experience";
import Lvl from "../components/lvl";
import Username from "../components/username";

class UpdatePlayer extends Component {
    state = {
        username: "",
        email: "",
        experience: 0,
        lvl: 0,
        showUsername: "",
        showEmail: "",
        showExperience: 0,
        showLvl: 0,
        showSummary: false,
        players: [
            {
                name: "asa",
                email: "asa@gmail.com",
                experience: 18,
                lvl: 6,
            },
            {
                name: "azi",
                email: "azi@gmail.com",
                experience: 22,
                lvl: 4,
            },
            {
                name: "byan",
                email: "byan@gmail.com",
                experience: 1,
                lvl: 10,
            },
        ],
    };

    handleFieldOnChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
            showSummary: false,
        });
    };

    handleSetDataPlayer = (event) => {
        var dataSearch = this.state.players.find(
            ({ name }) => name === event.target.value
        );
        this.setState({
            username: dataSearch.name,
            email: dataSearch.email,
            experience: dataSearch.experience,
            lvl: dataSearch.lvl,
            showUsername: "",
            showEmail: "",
            showExperience: 0,
            showLvl: 0,
            showSummary: false,
        });
    };

    handleShowSummary = () => {
        this.setState({
            username: "",
            email: "",
            experience: 0,
            lvl: 0,
            showUsername: this.state.username,
            showEmail: this.state.email,
            showExperience: this.state.experience,
            showLvl: this.state.lvl,
            showSummary: true,
        });
    };

    render() {
        return (
            <div class="container">
                <h1>Form Update Player</h1>
                <div class="container">
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="list-group">
                                {this.state.players.map((value, index) => {
                                    return (
                                        <button
                                            type="button"
                                            class="list-group-item list-group-item-action"
                                            onClick={this.handleSetDataPlayer}
                                            value={value.name}
                                        >
                                            {value.name}
                                        </button>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Username
                                </label>
                                <div class="col-sm-10">
                                    <Username
                                        name="username"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="username"
                                        value={this.state.username}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Email
                                </label>
                                <div class="col-sm-10">
                                    <Email
                                        name="email"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="email"
                                        value={this.state.email}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Experience
                                </label>
                                <div class="col-sm-10">
                                    <Experience
                                        name="experience"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="experience"
                                        value={this.state.experience}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">
                                    Lvl
                                </label>
                                <div class="col-sm-10">
                                    <Lvl
                                        name="lvl"
                                        onChange={this.handleFieldOnChange}
                                        placeholder="lvl"
                                        value={this.state.lvl}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button
                    class="btn btn-primary"
                    onClick={this.handleShowSummary}
                >
                    Submit
                </button>
                <br />
                {this.state.showSummary && (
                    <div class="container">
                        <div class="row">
                            <div class="offset-md-2 col-8">
                                <div class="row">
                                    <div class="col-5">Username</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.showUsername}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Email</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.showEmail}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Experience</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.showExperience}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Lvl</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.showLvl}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default UpdatePlayer;
