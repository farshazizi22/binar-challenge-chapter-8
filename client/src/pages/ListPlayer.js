import React, { Component } from "react";
import Username from "../components/username";

class CreatePlayer extends Component {
    state = {
        username: "",
        email: "",
        experience: 0,
        lvl: 0,
        searchValue: "",
        showSummarySearch: false,
        players: [
            {
                name: "asa",
                email: "asa@gmail.com",
                experience: 18,
                lvl: 6,
            },
            {
                name: "azi",
                email: "azi@gmail.com",
                experience: 22,
                lvl: 4,
            },
            {
                name: "byan",
                email: "byan@gmail.com",
                experience: 1,
                lvl: 10,
            },
        ],
    };

    handleFieldOnChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
            showSummarySearch: false,
        });
    };

    handleSearch = () => {
        var dataSearch = this.state.players.find(
            ({ name, email, experience, lvl }) =>
                name === this.state.searchValue ||
                email === this.state.searchValue ||
                experience === this.state.searchValue ||
                lvl === this.state.searchValue
        );

        this.setState({
            username: dataSearch.name,
            email: dataSearch.email,
            experience: dataSearch.experience,
            lvl: dataSearch.lvl,
            showSummarySearch: true,
        });
    };

    render() {
        return (
            <div>
                <h1>List Player</h1>
                <div class="container">
                    <div class="row">
                        <div class="offset-md-2 col-8">
                            <Username
                                name="searchValue"
                                onChange={this.handleFieldOnChange}
                                placeholder="username"
                                value={this.state.searchValue}
                            />
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" onClick={this.handleSearch}>
                    Search
                </button>
                <br />
                {this.state.showSummarySearch && (
                    <div class="container">
                        <div class="row">
                            <div class="offset-md-2 col-8">
                                <div class="row">
                                    <div class="col-5">Username</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.username}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Email</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">{this.state.email}</div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Experience</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">
                                        {this.state.experience}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">Lvl</div>
                                    <div class="col-2">:</div>
                                    <div class="col-4">{this.state.lvl}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Experience</th>
                            <th scope="col">Lvl</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.players.map((value, index) => {
                            return (
                                <tr>
                                    <th scope="row">{index + 1}</th>
                                    <td>{value.name}</td>
                                    <td>{value.email}</td>
                                    <td>{value.experience}</td>
                                    <td>{value.lvl}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CreatePlayer;
