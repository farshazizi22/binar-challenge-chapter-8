import React, { Component } from "react";

class Password extends Component {
    render() {
        const { name, onChange, placeholder, value } = this.props;
        return (
            <input
                type="password"
                className="form-control"
                onChange={onChange}
                name={name}
                placeholder={placeholder}
                value={value}
            />
        );
    }
}

export default Password;
