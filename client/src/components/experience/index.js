import React, { Component } from "react";

class Experience extends Component {
    render() {
        const { name, onChange, placeholder, value } = this.props;
        return (
            <input
                type="text"
                className="form-control"
                onChange={onChange}
                name={name}
                placeholder={placeholder}
                value={value}
            />
        );
    }
}

export default Experience;
