import "./App.css";
import { Component } from "react";
import CreatePlayer from "./pages/CreatePlayer";
import ListPlayer from "./pages/ListPlayer";
import UpdatePlayer from "./pages/UpdatePlayer";

class App extends Component {
    state = {
        activeMenu: "",
    };

    player = {
        username: "Azi",
    };

    handleMenuChange = (event) => {
        this.setState({
            activeMenu: event.target.id,
        });
    };

    render() {
        return (
            <div className="App">
                <div class="container-fluid">
                    <div class="row menu">
                        <div class="col-3"></div>
                        <div class="col-2">
                            <button
                                class="btn btn-primary"
                                id="createPlayer"
                                onClick={this.handleMenuChange}
                            >
                                Create Player
                            </button>
                        </div>
                        <div class="col-2">
                            <button
                                class="btn btn-primary"
                                id="updatePlayer"
                                onClick={this.handleMenuChange}
                            >
                                Update Player
                            </button>
                        </div>
                        <div class="col-2">
                            <button
                                class="btn btn-primary"
                                id="listPlayer"
                                onClick={this.handleMenuChange}
                            >
                                List Player
                            </button>
                        </div>
                    </div>
                </div>
                <br />
                {this.state.activeMenu === "createPlayer" && <CreatePlayer />}
                {this.state.activeMenu === "updatePlayer" && <UpdatePlayer />}
                {this.state.activeMenu === "listPlayer" && <ListPlayer />}
            </div>
        );
    }
}

export default App;
