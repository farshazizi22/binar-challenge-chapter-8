// setup database here, change the values to suit your environment
module.exports = {
    HOST: "localhost",
    USER: "farsha_azizi",
    PASSWORD: "admin",
    DB: "code_challenge_8",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
};
